package com.ksz.customerdal.dal.repos;

import org.springframework.data.repository.CrudRepository;

import com.ksz.customerdal.dal.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer,Long> {

}
